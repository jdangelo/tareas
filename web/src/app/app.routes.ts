
import { RouterModule, Routes } from '@angular/router';
import { TareasPendientesComponent } from './componentes/paginas/tareas-pendientes/tareas-pendientes.component';
import { TareasTerminadasComponent } from './componentes/paginas/tareas-terminadas/tareas-terminadas.component';
import { PaginaNoEncontradaComponent } from './componentes/paginas/pagina-no-encontrada.component';


const ROUTES: Routes = [
    { path: '', component: TareasPendientesComponent },
    { path: 'pendientes', component: TareasPendientesComponent },
    { path: 'terminadas', component: TareasTerminadasComponent },
    { path: '**', component: PaginaNoEncontradaComponent }
];

export const ROUTING = RouterModule.forRoot(ROUTES, { useHash: true });