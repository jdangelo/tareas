import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
//import { Tarea } from 'src/app/entidades/tarea';
import { TareasService } from '../../servicios/tareas.service';
import { Tarea } from '../../servicios/lbsdk';

@Component({
  selector: 'app-lista-tareas',
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.css']
})
export class ListaTareasComponent implements OnInit {

  constructor(
    private tareasService: TareasService
  ) { }

  @ViewChild('inputTarea') miInput: ElementRef;
  @Input() terminadas: boolean = true;
  tareas: Tarea[] = [];

  ngOnInit(): void {
    this.tareasService.find(this.terminadas)
      .subscribe((tareas: Tarea[]) => {
        this.tareas = tareas;
      })
  }
  // get tareas(){
  //   return this.tareasService.find(this.terminadas)
  // }

  agregarTarea(descripcion: string) {
    if (descripcion.trim().length > 0) {
      this.tareasService.create(descripcion)
        .subscribe((tarea: Tarea) => {
          this.tareas.unshift(tarea);
        })
    } else {
      alert("carga la tarea!!!")
    }
    this.miInput.nativeElement.value = '';
  }
  editarTarea(tarea: Tarea) {
    let estado = !tarea.terminada;
    tarea.terminada = estado;
    this.tareasService.edit(tarea)
      .subscribe((tarea: Tarea) => {
        console.log('Tarea actualizada', tarea);
        this.tareas = this.tareas.filter(t => t.id != tarea.id);
      })
  }

  borrarTarea(id: number) {
    this.tareasService.deleteById(id)
      .subscribe((res: any) => {
        if (res.count == 1) {
          this.tareas = this.tareas.filter(t => t.id != id);
        }
      })
  }

}
