export interface Tarea {
    id:number;
    descripcion:string;
    terminada:boolean;
    fechaCreacion:Date;
    eliminada?:boolean;
}
