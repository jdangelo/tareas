import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// Rutas
import { ROUTING } from './app.routes';
// Componentes
import { AppComponent } from './app.component';
import { ListaTareasComponent } from './componentes/lista-tareas/lista-tareas.component';
import { TareasPendientesComponent } from './componentes/paginas/tareas-pendientes/tareas-pendientes.component';
import { TareasTerminadasComponent } from './componentes/paginas/tareas-terminadas/tareas-terminadas.component';
import { PaginaNoEncontradaComponent } from './componentes/paginas/pagina-no-encontrada.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { TareasService } from './servicios/tareas.service';
import { SDKBrowserModule, TareaApi } from './servicios/lbsdk';


@NgModule({
  declarations: [
    AppComponent,
    ListaTareasComponent,
    TareasPendientesComponent,
    TareasTerminadasComponent,
    PaginaNoEncontradaComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ROUTING,
    SDKBrowserModule.forRoot()
  ],
  providers: [
    TareasService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
