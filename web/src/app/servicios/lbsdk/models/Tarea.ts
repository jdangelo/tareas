/* tslint:disable */

declare var Object: any;
export interface TareaInterface {
  "id"?: number;
  "descripcion": string;
  "terminada"?: boolean;
  "fechaCreacion": Date;
  "eliminada"?: boolean;
}

export class Tarea implements TareaInterface {
  "id": number;
  "descripcion": string;
  "terminada": boolean;
  "fechaCreacion": Date;
  "eliminada": boolean;
  constructor(data?: TareaInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Tarea`.
   */
  public static getModelName() {
    return "Tarea";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Tarea for dynamic purposes.
  **/
  public static factory(data: TareaInterface): Tarea{
    return new Tarea(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Tarea',
      plural: 'tareas',
      path: 'tareas',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "descripcion": {
          name: 'descripcion',
          type: 'string'
        },
        "terminada": {
          name: 'terminada',
          type: 'boolean'
        },
        "fechaCreacion": {
          name: 'fechaCreacion',
          type: 'Date'
        },
        "eliminada": {
          name: 'eliminada',
          type: 'boolean'
        },
      },
      relations: {
      }
    }
  }
}
