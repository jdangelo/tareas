/* tslint:disable */
import { Injectable } from '@angular/core';
import { Tarea } from '../../models/Tarea';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Tarea: Tarea,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
