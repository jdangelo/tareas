import { Injectable } from '@angular/core';
import { Tarea, TareaApi } from './lbsdk';


@Injectable()
export class TareasService {
  // private tareas: Tarea[] = [];
  // private ultimoId: number = 0;

  constructor(private tareasApi: TareaApi) {
    console.log('Servicio ok!!')
  }
  find(terminadas: boolean = false) {
    return this.tareasApi.find({ where: { terminada: terminadas }, order: 'fechaCreacion desc' })
  }
  // find(terminadas: boolean = false) {
  //   console.log('entró en find()')
  //   return this.tareas
  //     .filter(t => t.terminada == terminadas)
  //     .sort((a: Tarea, b: Tarea) => b.fechaCreacion.getTime() - a.fechaCreacion.getTime())
  // }

  create(descripcion: string) {
    let tarea = new Tarea();
    tarea.descripcion = descripcion;
    tarea.terminada = false;
    tarea.eliminada = false;
    return this.tareasApi.create(tarea)
  }
  // create(descripcion: string) {
  //   this.ultimoId += 1;
  //   let t: Tarea = {
  //     id: this.ultimoId,
  //     descripcion: descripcion,
  //     terminada: false,
  //     fechaCreacion: new Date()
  //   }
  //   this.tareas.push(t);
  // }
  edit(tarea: Tarea) {
    return this.tareasApi.updateAttributes(tarea.id,tarea)
  }

  deleteById(id: number) {
    return this.tareasApi.deleteById(id)
  }
  // deleteById(id: number) {
  //   this.tareas = this.tareas.filter(tarea => tarea.id != id)
  // }
}
